<?php


class DB
{
    protected $pdo;
    public $message;
    public $subNum;

    public function __construct()
    {
        require 'Config.php';

        try {
            $this->pdo = new PDO('mysql:host=' . $config["host"] . ';dbname=' . $config["database"], $config['username'], $config['password']);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function setSignupCount()
    {
        $stmt = $this->pdo->prepare('select * from signups');
        $stmt->execute();
        $this->subNum = count($stmt->fetchAll(PDO::FETCH_CLASS));
    }

    public function insertSignup($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) && $this->isEmailAvailable($email)) {

            $query = "INSERT INTO signups (email) VALUES('$email')";
            try {
                $stmt = $this->pdo->prepare($query);
                if ($stmt->execute()) {
                    $this->message = 'You have been signed up!';
                }
                $this->setSignupCount();
            } catch (Exception $e) {
                die($e->getMessage());
            }
        } else {
            $this->message = "Email address '$email' is considered invalid or is taken.\n";
        }
    }

    public function isEmailAvailable($email)
    {
        $stmt = $this->pdo->prepare("SELECT `email` FROM signups WHERE `email` = '" . $email . "' ");
        $stmt->execute();
        $count = count($stmt->fetchAll(PDO::FETCH_CLASS));

        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }
}
