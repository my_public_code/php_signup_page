<!-- Signup Template -->



<div class="card">
    <div class="card-title">
        <p>Enter your email to stay updated on launch information.</p>
    </div>
    <form method="POST" action="/" class="padding">
        <div class="form-field">
            <input type="email" name="email" placeholder="youremail@example.com" required>
        </div>
        <div class="form-field">
            <input type="submit" value="Put me on the list!">
        </div>
    </form>
    <div class="center padding">
        <p><?= $db->subNum ?> <?= $db->subNum > 1 || $db->subNum == 0 ? 'subscribers' : 'subscriber' ?> so far.</p>
    </div>
</div>