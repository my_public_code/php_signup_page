<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="lib/style.css">

    <title><?= $siteTitle ?></title>
</head>

<body>

    <main>
        <?php include('views/landing.php'); ?>
    </main>


    <footer class="d-flex align-items-center center padding">
        <div>
            <p><?= $siteTitle ?> | Comming in October | Sign up to be notified</p>
        </div>
    </footer>




</body>

</html>