<?php

require 'DB.php';

$db = new DB();
$db->setSignupCount();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if ($_POST["email"]) {
        $db->insertSignup($_POST["email"]);
    } else {
        die("Sorry. There was an error");
    }
}

$siteTitle = 'Your site\'s Title';

require 'views/layout.php';
